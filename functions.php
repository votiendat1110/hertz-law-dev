<?php
/*
 *  GLOBAL VARIABLES
 */
define('THEME_DIR', get_stylesheet_directory());
define('THEME_URL', get_stylesheet_directory_uri());

/*
 *  INCLUDED FILES
 */

$file_includes = [
    'inc/theme-assets.php',       // Style and JS
    'inc/theme-setup.php',        // General theme setting
    'inc/acf-options.php',        // ACF Option page
    'inc/theme-shortcode.php',    // Theme Shortcode
    'inc/class-bootstrap_navwalker.php'     // Bootrap Menu Class Walker
];

foreach ($file_includes as $file) {
    if ( !$filePath = locate_template($file) ) {
        trigger_error(sprintf(__('Missing included file'), $file), E_USER_ERROR);
    }

    require_once $filePath;
}

unset($file, $filePath);

/**
* GET CUSTOM TEMPLATE
*/ 
if( !function_exists( 'uiwp_get_template' ) ) {
  function uiwp_get_template( $path, $var = null, $return = false ) {
        if( ! is_child_theme() ) {
          $located = get_theme_root().'/'.get_template().'/'.$path;
        } else {
          $located = get_stylesheet_directory().'/'.$path;
        }

    if ( $var && is_array( $var ) )
      extract( $var );

    if( $return )
      { ob_start(); }

  // include file located
    include( $located );

    if( $return )
      return ob_get_clean();
  }
}

/**
* URL to point to website
*/ 
add_filter( 'login_headerurl', 'maple_login_url' );
function maple_login_url() {
  return home_url();
}

/**
* Custom Backend Footer
*/ 
function maple_custom_admin_footer() {
  $developer = "Maple Studio";
  $url = "http://maplestudio.vn";
  echo '<span id="footer-thankyou">' . __( 'Developed by', 'Maple Studio' ) . ' <a href="' . $url . '" target="_blank">' . $developer . '</a></span>';
}
/**
* Adding it to the admin area
*/
add_filter( 'admin_footer_text', 'maple_custom_admin_footer' );
 
/**
* Add body data to help identify current page
*/
if ( ! function_exists( 'kd_body_data' ) ) {
  function kd_body_data() {
    $type = '';
    $class = '';
    if ( is_front_page() || is_home() ) {
      $type = 'frontPage';
      $class = 'front-page';
    }
    echo 'class="' . $class . '" data-type="' . $type . '"';
  }
}
add_action( 'add_body_data', 'kd_body_data', 5 );

/**
* Get local image
*/
function get_image( $image ) {
  return get_template_directory_uri() . '/assets/images/' . $image;
}

/**
* Get image url by post ID
*/
function get_image_featured_url( $id, $size = 'full' ) {
  return wp_get_attachment_image_src( get_post_thumbnail_id( $id ), $size )[0];
}

/**
* Support SVG Images
*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
* Pagination
*/
function pagination_bar() {
  global $wp_query;

  $total_pages = $wp_query->max_num_pages;

  if ($total_pages > 1){
    $current_page = max(1, get_query_var('paged'));

    echo paginate_links(array(
        'base'      => get_pagenum_link(1) . '%_%',
        'format'    => '?paged=%#%',
        'current'   => $current_page,
        'total'     => $total_pages,
        'prev_next' => false,
    ));
  }
}

/**
 * Fix ACF Google Map
 * */
add_filter('acf/settings/google_api_key', function ($value) {
    return 'AIzaSyA3JFUYJJB7RQjXPjrUAYLmfsVJV_JWbPM';
});

if ( ! function_exists( 'vg_recently_viewed_product' ) ) {
  function vg_recently_viewed_product() {
    if ( ! is_singular( 'product' ) ) {
      return;
    }

    global $post;

    if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) )
      $viewed_products = array();
    else
      $viewed_products = (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] );

    if ( ! in_array( $post->ID, $viewed_products ) ) {
      $viewed_products[] = $post->ID;
    }

    if ( sizeof( $viewed_products ) > 15 ) {
      array_shift( $viewed_products );
    }

    // Store for session only
    wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
  }
  add_action( 'template_redirect', 'vg_recently_viewed_product' );
}

/**
 * Auto login after register
 * */
/*function auto_login_new_user( $user_id ) {
  wp_set_current_user($user_id);
  wp_set_auth_cookie($user_id);
}
add_action( 'user_register', 'auto_login_new_user' );*/
function vg_upload_dir( $dirs ) {
  $dirs['subdir'] = '/import';
  $dirs['path']   = $dirs['basedir'] . '/import';
  $dirs['url']    = $dirs['baseurl'] . '/import';

  return $dirs;
}

function my_cust_filename( $dir, $name, $ext ) {
  $newFileName = date( 'Ymdhis' ) . rand( 100, 999 ) . '.xls';

  return $newFileName;
}

/**
 * Your theme does not declare WooCommerce support
 * */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
  add_theme_support( 'woocommerce' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'wc-product-gallery-slider' );
  add_filter( 'woocommerce_enqueue_styles', '__return_false' );
}

// function woocommerce_single_product_summary_reorder() {
//   /**
//    * Hook: Woocommerce_single_product_summary.
//    *
//    * @hooked woocommerce_template_single_title - 5
//    * @hooked woocommerce_template_single_rating - 10
//    * @hooked woocommerce_template_single_price - 10
//    * @hooked woocommerce_template_single_excerpt - 20
//    * @hooked woocommerce_template_single_add_to_cart - 30
//    * @hooked woocommerce_template_single_meta - 40
//    * @hooked woocommerce_template_single_sharing - 50
//    * @hooked WC_Structured_Data::generate_product_data() - 60
//    */

//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
//   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

//   add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
//   //add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
//   add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
//   add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//   add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
//   //add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
//   add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

//   remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
//   remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
//   remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
// }
// woocommerce_single_product_summary_reorder();

// // Change the breadcrumb delimeter from '/' to '>'
// add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
// function wcc_change_breadcrumb_delimiter( $defaults ) {
//   $defaults['delimiter'] = ' &gt; ';
//   return $defaults;
// }

// remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
// remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

// // Custom Woocommerce Wrapper
// add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
// add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);
// function my_theme_wrapper_start() {
//     echo '<section id="qe-main">';
// }

// function my_theme_wrapper_end() {
//     echo '</section>';
// }

// // Remove shop page Count, Ordering, v.v...
// function woocommerce_remove_before_shop_loop() {
//   remove_action ( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
//   remove_action ( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
//   remove_action ( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
// }
// woocommerce_remove_before_shop_loop();

/*=============================================
=         REMOVE WORDPRESS LOGO              =
=============================================*/ 
add_action('wp_before_admin_bar_render', 'vg_admin_bar_remove', 0);
function vg_admin_bar_remove() {
  global $wp_admin_bar;

  /* Remove their stuff */
  $wp_admin_bar->remove_menu('wp-logo');
}

/*=============================================
=          ADMIN LOGIN PAGE LOGO             =
=============================================*/
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo() {
  ?>
    <style type="text/css">
      body.login div#login h1 a {
        background-image: url(<?php echo THEME_URL ?>/assets/images/logo.png);
        background-size: 100%;
        /*height: 195px;*/
        width: 260px;
        /*margin: -60px auto -27px;*/
      }
    </style>
  <?php
}

/*=============================================
=            GET ATTACHMENT DATA             =
=============================================*/
function vg_get_attachment( $attachment_id, $size = '' ) {
  $attachment = get_post( $attachment_id );
  if( $size == '' ) {
    $imgSrc = $attachment->guid;
  } else {
    $imgSrc = wp_get_attachment_image_src( $attachment_id, $size )[0];
  }
  return array(
      'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
      'caption' => $attachment->post_excerpt,
      'description' => $attachment->post_content,
      'href' => get_permalink( $attachment->ID ),
      'src' => $imgSrc,
      'title' => $attachment->post_title
  );
}

/*=============================================
=            BREADCRUMBS                  =
=============================================*/
//  to include in functions.php
function the_breadcrumb() {
    $sep = ' <i class="fa fa-angle-double-right"></i> ';
    if (!is_front_page()) {
  
  // Start the breadcrumb with a link to your homepage
        echo '<div class="breadcrumbs">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        echo 'Homepage';
        // bloginfo('name');
        echo '</a>' . $sep;
        
        get_ancestors( $term_id, 'taxonomy' );
  
  // Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            the_category('title_li=');
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                _e( 'Blog Archives', 'text_domain' );
            }
        }
  
  // If the current page is a single post, show its title with the separator
        if (is_single()) {
            echo $sep; ?>
            <a><?php the_title(); ?></a>
        <?php
        }
  
  // If the current page is a static page, show its title.
        if (is_page()) {
            //echo the_title();?>
            <a><?php echo the_title(); ?></a>
        <?php
        }
  
  // if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }

        if (is_404()){
        ?>
          <a>Error 404</a>
        <?php
        }
        echo '</div>';
    }
}

/*=============================================
=            GET POST VIEWS                  =
=============================================*/
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
?>