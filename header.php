<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title( '', true, '' ); ?></title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Font Roboto & Raleway -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700|Roboto:300,400,700&amp;subset=vietnamese" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
    <header>
        <div class="hl-header-topbar">
            <div class="hl-container overflow-hidden">
                <div class="topbar-box">
                    <a class="hl-topbar-phone" href="tel:<?php the_field('phone', 'option') ?>"><i class="fas fa-phone"></i> <?php the_field('phone', 'option') ?></a>
                    <a class="hl-topbar-location" href="<?php the_field('address_url', 'option') ?>"><i class="fas fa-map-marker-alt"></i> <?php the_field('address', 'option') ?></a>
                </div>

                <div class="topbar-box">
                    <?php if( get_field('facebook', 'option') ): ?>
                        <a href="<?php the_field('facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a>
                    <?php endif; ?>

                    <?php if( get_field('google_plus', 'option') ): ?>
                       <a href="<?php the_field('google_plus', 'option'); ?>"><i class="fab fa-google-plus-g"></i></a>
                    <?php endif; ?>

                    <?php if( get_field('twitter', 'option') ): ?>
                        <a href="<?php the_field('twitter', 'option'); ?>"><i class="fab fa-twitter"></i></a>
                    <?php endif; ?>
                    
                    <?php if( get_field('linked_in', 'option') ): ?>
                        <a href="<?php the_field('linked_in', 'option'); ?>"><i class="fab fa-linkedin-in"></i></a>
                    <?php endif; ?>    
                </div>
            </div>
        </div>

        <div class="hl-header-main">
            <div class="hl-container">
                <a class="hl-logo pos-r block" href="/">
                    <div class="hl-logo-img pos-a bg-cover" style="background-image: url('<?php the_field('logo', 'option'); ?>');"></div>
                </a>

                <div class="hl-nav-container pos-r">
                    <div class="hl-nav-img pos-a">
                        <img src="<?php the_field('nav_menu_image', 'option'); ?>">
                    </div>

                    <?php 
                        wp_nav_menu( array(
                            'menu'           => 'Main Menu',
                            'theme_location' => 'main_menu',
                            'menu_id'        => 'Main_Menu',
                            'container'      => 'ul',
                            'fallback_cb'    => false
                        ) );
                    ?>

                    <div id="hl-nav-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="MobileMenu">
        <?php 
            wp_nav_menu( array(
                'menu'           => 'Main Menu',
                'theme_location' => 'main_menu',
                'menu_id'        => 'Main_Menu',
                'container'      => 'ul',
                'fallback_cb'    => false
            ) );
        ?>
    </div>