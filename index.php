<?php get_header(); ?>
<div id="PageTitle">
    <div class="page-title-bg pos-r block">
        <div class="hl-pagetitle-img pos-a bg-cover" style="background-image: url('<?php echo THEME_URL ?>/assets/images/default-pagetitle.jpg');"></div>
        <div class="hl-pagetitle-title pos-a">
            <h2>Articles</h2>
            <div class="hl-pagetitle-breadcrumb">
                <?php //the_breadcrumb(); ?>
                <?php 
                    if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }
                ?>
            </div>       
        </div>
    </div>
</div>

<div id="PostsPage" class="hl-container">
    <div class="hl-posts-container">
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $args_query = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'paged' => $paged,
                'order' => 'DESC',
            );
            $the_posts = new WP_Query( $args_query );
        ?>

        <?php if ( $the_posts->have_posts() ) : ?>
            <?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
                <article class="hl-post block">
                    <a class="hl-post-title block" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <div class="hl-post-excerpt"><?php the_excerpt(); ?></div>
                    <a class="hl-post-link block" href="<?php the_permalink(); ?>">Read more <i class="fas fa-long-arrow-alt-right"></i></a>
                </article>
            <?php endwhile; ?>

            <!-- Posts Pagitation -->
            <div class="hl-pagination">
                <?php
                    pagination_bar();
                ?>
            </div>
        <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>
