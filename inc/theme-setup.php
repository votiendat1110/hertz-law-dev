<?php
function theme_setup()
{
  // Language loading
  // load_theme_text_domain('Theme Domain', trailingslashit( get_template_directory()).'languages' );

  // HTML5 support; mainly here to get rid of some nasty default styling that WordPress used to inject
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  /*
   * Let WordPress manage the document title.
   * By adding theme support, we declare that this theme does not use a
   * hard-coded <title> tag in the document head, and expect WordPress to
   * provide it for us.
   */
  add_theme_support( 'title-tag' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  add_theme_support( 'post-thumbnails' );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
  ) );

  /*
   * Enable support for Post Formats.
   *
   * See: https://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
  ) );

  /*
   * Register Menus
   */
  register_nav_menu( 'main_menu', __( 'Main Menu', 'ROTheme' ) );

}
add_action( 'after_setup_theme', 'theme_setup', 11);

function limit_text($text, $limit) {

  if (str_word_count($text, 0) > $limit) {
    $words = str_word_count($text, 2);
    $pos = array_keys($words);
    $text = substr($text, 0, $pos[$limit]) . '...';
  }

  return $text;
}

/*
 * Add category for [CTP]
 */
add_action( 'init', 'create_private_tax' );
function create_private_tax() {
  register_taxonomy(
    'Portfolio Categories', 
    'portfolio',
    array("hierarchical" => true,
      "label" => "Portfolio Categories",
      "singular_label" => "Portfolio Category",
      'update_count_callback' => '_update_post_term_count',
      'query_var' => true,
      'rewrite' => array( 'slug' => 'portfolio-category', 
      'with_front' => false ),
      'public' => true,
      'show_ui' => true,'show_tagcloud' => true,
      '_builtin' => false,
      'show_in_nav_menus' => false
    )
  );

  register_taxonomy(
    'Team Categories', 
    'team',
    array("hierarchical" => true,
      "label" => "Team Categories",
      "singular_label" => "Team Category",
      'update_count_callback' => '_update_post_term_count',
      'query_var' => true,
      'rewrite' => array( 'slug' => 'team-category', 'with_front' => false ),
      'public' => true,
      'show_ui' => true,
      'show_tagcloud' => true,
      '_builtin' => false,
      'show_in_nav_menus' => false
    )
  );

  register_taxonomy(
    'Service Categories', 
    'service',
    array("hierarchical" => true,
      "label" => "Service Categories",
      "singular_label" => "Service Category",
      'update_count_callback' => '_update_post_term_count',
      'query_var' => true,
      'rewrite' => array( 'slug' => 'service-category', 
      'with_front' => false ),
      'public' => true,
      'show_ui' => true,'show_tagcloud' => true,
      '_builtin' => false,
      'show_in_nav_menus' => false
    )
  );

  register_taxonomy(
    'Testimonial Categories', 
    'testimonial',
    array("hierarchical" => true,
      "label" => "Testimonial Categories",
      "singular_label" => "Testimonial Category",
      'update_count_callback' => '_update_post_term_count',
      'query_var' => true,
      'rewrite' => array( 'slug' => 'testimonial-category', 
      'with_front' => false ),
      'public' => true,
      'show_ui' => true,'show_tagcloud' => true,
      '_builtin' => false,
      'show_in_nav_menus' => false
    )
  );
}

/*
 * [CPT] Portfolio
 */
function cpt_portfolio()
{
  $label = array(
    'name' => 'Portfolios',
    'singular_name' => 'Portfolio'
  );

  $args = array(
    'labels' => $label,
    'description' => '',
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    'taxonomies' => array( 'post_tag' ),
    'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 5,
    'menu_icon' => '/wp-content/themes/HertzLaw/assets/images/briefcase.png',
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post'
  );
  register_post_type('portfolio', $args);
}
add_action('init', 'cpt_portfolio');

/*
 * [CPT] Service
 */
function cpt_service()
{
  $label = array(
    'name' => 'Services',
    'singular_name' => 'Service'
  );

  $args = array(
    'labels' => $label,
    'description' => '',
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    'taxonomies' => array( 'post_tag' ),
    'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 5,
    'menu_icon' => '/wp-content/themes/HertzLaw/assets/images/customer.png',
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post'
  );
  register_post_type('service', $args);
}
add_action('init', 'cpt_service');

/*
 * [CPT] Team
 */
function cpt_team()
{
  $label = array(
    'name' => 'Teams',
    'singular_name' => 'Team'
  );

  $args = array(
    'labels' => $label,
    'description' => '',
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    'taxonomies' => array( 'post_tag' ),
    'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 5,
    'menu_icon' => '/wp-content/themes/HertzLaw/assets/images/group.png',
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post'
  );
  register_post_type('team', $args);
}
add_action('init', 'cpt_team');

/*
 * [CPT] Testimonial
 */
function cpt_testimonial()
{
  $label = array(
    'name' => 'Testimonials',
    'singular_name' => 'Testimonial'
  );

  $args = array(
    'labels' => $label,
    'description' => '',
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    'taxonomies' => array( 'post_tag' ),
    'hierarchical' => false,
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 5,
    'menu_icon' => '/wp-content/themes/HertzLaw/assets/images/testimonials.png',
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'capability_type' => 'post'
  );
  register_post_type('testimonial', $args);
}
add_action('init', 'cpt_testimonial');
?>