<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title-single.php', $atts ); ?>

<div id="SingPost" class="hl-container">
	<div class="hl-post-title-container">
		<div class="hl-post-title raleway"><?php the_title(); ?></div>
		<?php setPostViews(get_the_ID()); ?>
		<div class="hl-post-views"><i class="fas fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?></div>
	</div>

	<div class="hl-post-content">
		<?php
			$content = apply_filters('the_content', $post->post_content); 
			echo $content;  
		?>
	</div>
	
	<div class="hl-next-prev-post">
		<div class="prev-post">
			<span><i class="fas fa-long-arrow-alt-left"></i> <?php _e('Previous'); ?></span><br>
			<?php previous_post_link(); ?></div>
		<div class="next-post">
			<span><?php _e('Next'); ?> <i class="fas fa-long-arrow-alt-right"></i></span><br>
			<?php next_post_link(); ?>
		</div>
	</div>

	<!--  comment  -->
	<?php //echo comments_template() ?>
</div>
<?php get_footer(); ?>