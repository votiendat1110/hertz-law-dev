<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title.php', $atts ); ?>

<div id="Front-page">
	<!-- PORTFOLIO SECTION -->
	<div class="hl-home-portfolio hl-home-section">
		<div class="hl-section-title-box">
			<div class="hl-section-sub-title raleway"><?php the_field('portfolio_section_sub_title'); ?></div>
			<div class="hl-section-title raleway"><?php the_field('portfolio_section_title'); ?></div>
		</div>
		
		<div class="hl-portfolio-container">
			<?php $args_query = array(
			        'post_type' 		=> 'portfolio',
			        'post_status' 		=> 'publish',
			        'posts_per_page' 	=> 4,
			        'order'				=> 'DESC',
		        );
		        $the_posts = new WP_Query( $args_query ); 
	    	?>

	    	<?php if ( $the_posts->have_posts() ) : ?>
	    		<?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
					<div class="hl-portfolio-box pos-r block">
						<div class="hl-portfolio-img-box block pos-r">
							<div class="hl-portfolio-img bg-cover pos-a" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></div>
						</div>
						<div class="hl-portfolio-content">
							<div class="hl-portfolio-manager">
								<?php 
									$porfolio_manager = get_field('portfolio_manager'); 
									echo $porfolio_manager[0]->post_title; 
								?>		
							</div>
							<a class="hl-portfolio-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>

						<div class="hl-portfolio-overlay pos-a bg-cover">
							<div class="hl-portfolio-content pos-a">
								<div class="hl-portfolio-manager">
									<?php 
										$porfolio_manager = get_field('portfolio_manager'); 
										echo $porfolio_manager[0]->post_title; 
									?>		
								</div>
								<div class="hl-portfolio-title"><?php the_title(); ?></div>
								<a href="<?php the_permalink(); ?>"><i class="fas fa-search"></i></a>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
	
	<!-- LAWYER SECTION -->
	<div class="hl-home-team hl-home-lawyer hl-home-section hl-container">
		<div class="hl-section-title-box">
			<div class="hl-section-sub-title"><?php the_field('lawyer_section_sub_title'); ?></div>
			<div class="hl-section-title"><?php the_field('lawyer_section_title'); ?></div>
		</div>

		<div class="hl-team-container">
			<?php $args_query = array(
			        'post_type' 		=> 'team',
			        'post_status' 		=> 'publish',
			        'posts_per_page' 	=> 6,
			        'order'				=> 'DESC',
		        );
		        $the_posts = new WP_Query( $args_query );
	    	?>
	    	<?php if ( $the_posts->have_posts() ) : ?>
	    		<?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
	    			<div class="hl-team-box pos-r block">
						<div class="hl-team-img-box block pos-r">
							<div class="hl-team-img bg-cover pos-a" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></div>
							<div class="hl-team-overlay pos-a bg-cover"></div>
							<!-- <a class="hl-arrow block pos-a center-pos-a" href="<?php //the_permalink(); ?>"><img src="<?php //echo THEME_URL ?>/assets/images/next-page.svg"></a> -->
						</div>
						
						<div class="hl-team-content">
							<a class="hl-team-name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<div class="hl-team-cat">
								<?php
									$postcat = get_the_terms( get_post()->ID, 'Team Categories' );
									echo $postcat[0]->name; 
								?>
							</div>
							<a class="hl-team-btn" href="<?php the_permalink(); ?>">Learn more</a>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
	
	<!-- STAFF SECTION -->
	<div class="hl-home-team hl-home-staff hl-home-section hl-container">
		<div class="hl-section-title-box">
			<div class="hl-section-sub-title"><?php the_field('staff_section_sub_title'); ?></div>
			<div class="hl-section-title"><?php the_field('staff_section_title'); ?></div>
		</div>

		<div class="hl-team-container">
			<?php $args_query = array(
			        'post_type' 		=> 'team',
			        'post_status' 		=> 'publish',
			        'posts_per_page' 	=> 6,
			        'order'				=> 'DESC',
		        );
		        $the_posts = new WP_Query( $args_query );
	    	?>
	    	<?php if ( $the_posts->have_posts() ) : ?>
	    		<?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
	    			<div class="hl-team-box pos-r block">
						<div class="hl-team-img-box block pos-r">
							<div class="hl-team-img bg-cover pos-a" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></div>
							<div class="hl-team-overlay pos-a bg-cover"></div>
							<!-- <a class="hl-arrow block pos-a center-pos-a" href="<?php //the_permalink(); ?>"><img src="<?php //echo THEME_URL ?>/assets/images/next-page.svg"></a> -->
						</div>
						
						<div class="hl-team-content">
							<a class="hl-team-name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<div class="hl-team-cat">
								<?php
									$postcat = get_the_terms( get_post()->ID, 'Team Categories' );
									echo $postcat[0]->name; 
								?>
							</div>
							<a class="hl-team-btn" href="<?php the_permalink(); ?>">Learn more</a>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php else : ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
	
	<!-- PARTNERS SECTION -->
	<div class="hl-home-partner hl-container hl-home-section">
		<?php
		if( have_rows('partner_group') ):
			
			while ( have_rows('partner_group') ) : the_row();
		?>
			<img class="hl-partner" src="<?php the_sub_field('partner_logo'); ?>">
		<?php
		    endwhile;

		else :

		    // no rows found

		endif;
		?>
	</div>
</div>

<?php get_footer(); ?>
