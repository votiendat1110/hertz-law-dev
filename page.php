<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title.php', $atts ); ?>

<div id="Page" class="hl-container">
	<?php
		$content = apply_filters('the_content', $post->post_content); 
		echo $content;  
	?>
</div>

<?php get_footer(); ?>