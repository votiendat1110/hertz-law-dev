<div id="PageTitle">
    <div class="page-title-bg pos-r block">
        <div class="hl-pagetitle-img pos-a bg-cover" style="background-image: url('<?php echo THEME_URL ?>/assets/images/default-pagetitle.jpg');"></div>
        <div class="hl-pagetitle-title pos-a">
            <h2><?php the_title(); ?></h2>
            <div class="hl-pagetitle-breadcrumb">
				<?php //the_breadcrumb(); ?>
				<?php 
					if(function_exists('bcn_display'))
				    {
				        bcn_display();
				    }
				?>
			</div>       
        </div>
    </div>
</div>