<div id="PageTitle">
	<?php if( get_field('page_title_style') == 'bg-img' ): ?>
		<div class="page-title-bg pos-r block">
			<div class="hl-pagetitle-img pos-a bg-cover" style="background-image: url('<?php the_field('page_title_background_image'); ?>');"></div>
			<div class="hl-pagetitle-title pos-a">
				<h2><?php the_title(); ?></h2>
				
				<div class="hl-pagetitle-breadcrumb">
					<?php //the_breadcrumb(); ?>
					<?php 
						if(function_exists('bcn_display'))
					    {
					        bcn_display();
					    }
					?>
				</div>		
			</div>
		</div>
	<?php else:

		the_field('page_title_slider');

	?>
	<?php endif; ?>
</div>