<div id="OurTeam" class="hl-container">
	<div class="hl-section-title"><?php the_field('team_title'); ?></div>
	
	<div class="hl-team-container">
		<?php 
			$cat = get_field('team_category');

			$args_query = array(
		        'post_type' 		=> 'team',
		        'post_status' 		=> 'publish',
		        'posts_per_page' 	=> 6,
		        'tax_query' 		=> array(
			        array(
			            'taxonomy' 	=> $cat[0]->taxonomy,   // taxonomy name
			            'terms' 	=> $cat[0]->term_id,       // term id, term slug or term name
			        )
			    ),
		        'order'				=> 'DESC',
	        );
	        $the_posts = new WP_Query( $args_query );
    	?>
    	<?php if ( $the_posts->have_posts() ) : ?>
    		<?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
    			<div class="hl-team-box pos-r block">
					<div class="hl-team-img-box block pos-r">
						<div class="hl-team-img bg-cover pos-a" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></div>
						<div class="hl-team-overlay pos-a bg-cover"></div>
					</div>
					
					<div class="hl-team-content">
						<a class="hl-team-name" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<div class="hl-team-cat">
							<?php
								$postcat = get_the_terms( get_post()->ID, 'Team Categories' );
								echo $postcat[0]->name; 
							?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		<?php else : ?>
		    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	</div>
</div>