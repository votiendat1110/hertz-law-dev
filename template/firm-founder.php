<div id="FirmFounder">
	<div class="hl-section-title">Firm Founder</div>

	<div class="hl-founder-container">
		<div class="hl-founder-img"><img src="<?php the_field('founder_image'); ?>"></div>
		<div class="hl-founder-content"><?php the_field('founder_content'); ?></div>
	</div>
</div>