<?php /* Template Name: Testimonials */ ?>

<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title.php', $atts ); ?>

<div id="Testimonial" class="hl-container hl-space">
    <div class="hl-section-title">OUR TESTIMONIALS</div>

    <div class="hl-posts-container">
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $args_query = array(
                'post_type' => 'testimonial',
                'post_status' => 'publish',
                'paged' => $paged,
                'order' => 'DESC',
            );
            $the_posts = new WP_Query( $args_query );
        ?>

        <?php if ( $the_posts->have_posts() ) : ?>
            <?php while ( $the_posts->have_posts() ) : $the_posts->the_post(); ?>
                <article class="hl-post block">
                    <div class="hl-post-content">
				       	<?php
							$content = apply_filters('the_content', $post->post_content); 
							echo $content;  
						?>
					</div>

                    <div class="hl-post-title block">- <?php the_title(); ?></div>
                </article>
            <?php endwhile; ?>

            <!-- Posts Pagitation -->
            <div class="hl-pagination">
                <?php
                    pagination_bar();
                ?>
            </div>
        <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>