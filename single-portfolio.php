<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title-single.php', $atts ); ?>

<div id="SinglePortfolio" class="hl-container">
	<div class="hl-portfolio-col">
		<img src="<?php the_post_thumbnail_url( 'full' ); ?>">
		<!-- <div class="hl-portfolio-img-box pos-r">
			<div class="hl-portfolio-img pos-a bg-cover" style="background-image: url('<?php //the_post_thumbnail_url( 'full' ); ?>');"></div>
		</div> -->
	</div>

	<div class="hl-portfolio-col">
		<h3 class="hl-portfolio-title"><?php the_title(); ?></h3>
		
		<?php $portfolio_manager = get_field('portfolio_manager'); ?>
		<a class="hl-portfolio-manager" href="<?php echo $portfolio_manager[0]->guid; ?>"><i class="fas fa-user"></i> <?php echo $portfolio_manager[0]->post_title; ?></a>

		<div class="hl-portfolio-content">
			<?php
				$content = apply_filters('the_content', $post->post_content); 
				echo $content;  
			?>

			<?php if( get_field('enable_button') === true ): ?>
				<a class="hl-portfolio-btn" href="<?php the_field('buy_now_url'); ?>" target="_blank"><?php _e('Buy Now'); ?><i class="fa fa-angle-right"></i></a>
			<?php else:

			?>
			<?php endif; ?>
		</div>

		<!--  comment  -->
		<?php //echo comments_template() ?>
	</div>
</div>
<?php get_footer(); ?>