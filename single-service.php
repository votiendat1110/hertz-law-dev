<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title-single.php', $atts ); ?>

<div id="SinglePortfolio" class="hl-container">
	<div class="hl-portfolio-col">
		<img src="<?php the_post_thumbnail_url( 'full' ); ?>">
	</div>

	<div class="hl-portfolio-col">
		<!-- <h3 class="hl-portfolio-title"><?php //the_title(); ?></h3> -->

		<div class="hl-portfolio-content">
			<?php
				$content = apply_filters('the_content', $post->post_content); 
				echo $content;  
			?>
		</div>

		<!--  comment  -->
		<?php //echo comments_template() ?>
	</div>
</div>
<?php get_footer(); ?>