<?php /* Template Name: About Team */ ?>

<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title.php', $atts ); ?>

<div id="AboutTeam" class="hl-container">
    <?php uiwp_get_template( 'template/firm-founder.php', $atts ); ?>

    <?php uiwp_get_template( 'template/our-team.php', $atts ); ?>
</div>

<?php get_footer(); ?>