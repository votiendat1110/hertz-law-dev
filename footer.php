<footer>
	<div class="hl-container">
		<div class="hl-footer-logo">
			<a class="hl-footer-logo-box pos-r block" href="/">
				<div class="hl-footer-logo-img pos-a bg-cover" style="background-image: url('<?php the_field('footer_logo', 'option'); ?>');"></div>
			</a>

			<div class="hl-footer-social">
				<?php if( get_field('facebook', 'option') ): ?>
                    <a href="<?php the_field('facebook', 'option'); ?>"><i class="fab fa-facebook-f"></i></a>
                <?php endif; ?>

                <?php if( get_field('google_plus', 'option') ): ?>
                   <a href="<?php the_field('google_plus', 'option'); ?>"><i class="fab fa-google-plus-g"></i></a>
                <?php endif; ?>

                <?php if( get_field('twitter', 'option') ): ?>
                    <a href="<?php the_field('twitter', 'option'); ?>"><i class="fab fa-twitter"></i></a>
                <?php endif; ?>
                
                <?php if( get_field('linked_in', 'option') ): ?>
                    <a href="<?php the_field('linked_in', 'option'); ?>"><i class="fab fa-linkedin-in"></i></a>
                <?php endif; ?>
			</div>
		</div>

		<div class="hl-footer-widget">
			<?php
			if( have_rows('footer_widget', 'option') ):
			    while ( have_rows('footer_widget', 'option') ) : the_row();
			?>
				<div class="hl-widget block">
					<div class="hl-widget-title"><?php the_sub_field('widget_title', 'option'); ?></div>
					<div class="hl-widget-content"><?php the_sub_field('widget_content', 'option'); ?></div>
				</div>
			<?php
			    endwhile;

			else :

			    // no rows found

			endif;
			?>
		</div>

		<div class="hl-footer-copyright"><?php the_field('footer_copyright', 'option'); ?></div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>