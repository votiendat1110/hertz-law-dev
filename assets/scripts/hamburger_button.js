module.exports = {
  	init: function() {
		var self = this;

		$(document).ready(function(){
			$MobileMenu = $('#MobileMenu');	

			$('#hl-nav-icon').click(function(){
				$(this).toggleClass('open');
				$MobileMenu.toggleClass('activeMobileMenu');
			});

			$MobielMenuHasChildren = $('#MobileMenu ul .menu-item-has-children');
			$('#MobileMenu ul .menu-item-has-children a').removeAttr("href");	
			$MobielMenuHasChildren.click(function(){
				$(this).toggleClass('active-mobile-sub-menu');
			});
		});;
	},
};