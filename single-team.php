<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title-single.php', $atts ); ?>

<div id="SingleTeam" class="hl-container">
	
	<div class="hl-team">
		<div class="hl-team-avatar pos-r">
			<div class="hl-team-avatar-img pos-a bg-cover" style="background-image: url('<?php the_post_thumbnail_url( 'full' ); ?>');"></div>
		</div>
		<div class="hl-team-desc">
			<h3><?php the_title(); ?></h3>
			<div class="hl-team-desc-content"><?php the_field('short_description'); ?></div>
		</div>
	</div>

	<div class="hl-team-content">
		<?php
			$content = apply_filters('the_content', $post->post_content); 
			echo $content;  
		?>
	</div>

	<div class="hl-border"></div>
</div>
<?php get_footer(); ?>