<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>
<?php uiwp_get_template( 'template/page-title.php', $atts ); ?>

<div id="Contact">
	<div class="hl-container">
		<div class="hl-section-title">CONTACT INFO</div>

	    <div class="hl-main-contact-container">
	    	<div class="hl-phone-extensions"><?php the_field('phone_extentions'); ?></div>
	    	<div class="hl-main-contact">
	    		<p>Main office phone
	    			<a href="tel:<?php the_field('phone'); ?>"><i class="fas fa-phone"></i> <?php the_field('phone'); ?></a>
	    		</p>
	    		
	    		<p>Fax
					<a href="tel:<?php the_field('fax'); ?>"><i class="fas fa-fax"></i> <?php the_field('fax'); ?></a>
	    		</p>
	    		
				<p>Address
					<a href="<?php the_field('address_url', 'option'); ?>"><i class="fas fa-map-marker-alt"></i> <?php the_field('address'); ?></a>
				</p>	
	    	</div>    	
	    </div>
    </div>

    <div class="hl-contact-form-container pos-r">
    	<div class="hl-contact-form-bg pos-a bg-cover" style="background-image: url('<?php the_field('form_background'); ?>');"></div>
    	<div class="hl-contact-form-shortcode"><?php the_field('contact_form'); ?></div>
    </div>
</div>	

<?php get_footer(); ?>