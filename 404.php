<?php get_header(); ?>
<div id="PageTitle">
	<div class="page-title-bg pos-r block">
		<div class="hl-pagetitle-img pos-a bg-cover" style="background-image: url('<?php echo THEME_URL ?>/assets/images/default-pagetitle.jpg');"></div>
		<div class="hl-pagetitle-title pos-a">
			<h2>404 Error Page</h2>
			<div class="hl-pagetitle-breadcrumb"><?php the_breadcrumb(); ?></div>		
		</div>
	</div>
</div>

<div id="Error">
	<div class="kf_404_wrap">
				<h2>4<strong> {0} </strong>4</h2>
				<h4>opps</h4>
				<h5>We can"t seem to find the page <strong>You"r looking for.</strong></h5>
				<p>You can use the form below to search for what you need.</p>                    
				<div class="kf_404_form">
					<div class="widget widget-search">
	<form class="kode-search" method="get" id="kode-searchform" action="http://hertz-law.com//">
			<input type="text" placeholder="Search Here" name="s" id="s" autocomplete="off" data-default="Type keywords...">
	<label><input type="submit" value=""></label>
  </form>
</div>				</div>
				<div class="kf_page_list">
					<p>You can browse the following links that may help you for what you are looking.</p>
				   	
				</div>
			</div>
</div>

<?php get_footer(); ?>
